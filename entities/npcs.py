from functools import partial
from logging import getLogger

import pyxel
from matplotlib.patches import CirclePolygon

from config import HITBOX_COLOR, SHOW_TRIGGERS
from engine.character import Character
from engine.effects.dialogue import Dialogue
from engine.helpers.structs import Coords, Hitbox, NPCInfo, InfoAction, Event, EventType
from resources.animations.characters import GOBLIN_STANDING
from resources.quests.intro import GOBLIN_FIRST_QUEST
from resources.texts.hints import PRESS_TO_TALK
from resources.texts.quests import GOBLIN_FIRST_MISSION
from resources.tiles.text_box import DEFAULT_TEXTBOX_TILESET

logger = getLogger('Goblin')

class Goblin(Character):
    HITBOX_REL_PTS = [
        Coords(2,2), Coords(3,1), Coords(6,1),
        Coords(6,2),
        Coords(2,8), Coords(6,8)
    ]
    INFO=NPCInfo(radius=20, text=PRESS_TO_TALK, action=InfoAction([
        partial(Dialogue, tiles=DEFAULT_TEXTBOX_TILESET, text=GOBLIN_FIRST_MISSION,
                on_finish_action=Event(type=EventType.START_QUEST, target=GOBLIN_FIRST_QUEST))
    ]), trigger_keys=[pyxel.KEY_ENTER])

    def __init__(self, position):
        super().__init__({
            str(GOBLIN_STANDING) : GOBLIN_STANDING}, 
            position, 'Arien', hitbox=Hitbox(position, self.HITBOX_REL_PTS, 'GOBLIN_HTBX'))
        self.animation = GOBLIN_STANDING
        self.info_circle = CirclePolygon((self.position+Coords(4,4)).tuple, radius=self.INFO.radius)
        logger.debug(self.info_circle)

        self.info_circle_path = self.info_circle.get_path()

    @property
    def vertices(self):
        return self.info_circle_path.vertices

    def info_trigger_path(self, points):
        return self.info_circle.contains_points(points)

    def draw_info_trigger(self, cam_pos: Coords):
        if not SHOW_TRIGGERS:
            return
        last_v = None
        for v in self.vertices:
            pos = self.position+Coords(4,4)
            v = [(v[0] * self.INFO.radius) - cam_pos.x + pos.x, (v[1] * self.INFO.radius) - cam_pos.y +pos.y]

            # logger.info(f'Drawing {v}')
            if last_v is not None:
                pyxel.line(last_v[0], last_v[1], v[0], v[1], int(HITBOX_COLOR))
            last_v = v

    def check_if_triggered(self):
        for btn in self.INFO.trigger_keys:
            if pyxel.btnp(btn):
                return True
