from enum import Enum
from logging import getLogger
from random import randint

import pyxel

from config import MUTE_SOUNDS, LOG_PATHFINDING
from engine.character import Character, WithSight
from engine.helpers.structs import Coords, Hitbox, EventType, MapSkeleton, MapTileType
from engine.movement import Movement, StepType
from entities.hero import Arien
from resources.animations.characters.slime import SLIME_ALL, SLIME_DEAD
from resources.sounds import SOUNDS

from pathfinding.core.diagonal_movement import DiagonalMovement
from pathfinding.core.grid import Grid
from pathfinding.finder.a_star import AStarFinder

logger = getLogger('Enemies')

class HostilityLevel(Enum):
    FRIENDLY = 0
    TERRITORIAL = 1 #TODO: Implement behaviour
    AGGRESSIVE = 2

class Enemy(WithSight):
    hostility: HostilityLevel
    attacking: bool = False
    attacked_by_player: bool = False
    player_in_sight: bool = False

    def check_if_player_in_sight(self, player: Arien, _map: MapSkeleton) -> bool:
        return self.check_if_coords_in_sight(player.middle, _map)

    def get_path_to_player(self, player: Arien, _map: MapSkeleton):
        scaled_map = _map.as_custom_list(scale=2, swap_x_y=True)
        grid = Grid(matrix=scaled_map)
        self_map_pos = self.middle.x // 8, self.middle.y // 8
        player_map_pos = player.middle.x // 8, player.middle.y // 8
        start = grid.node(*self_map_pos)
        end = grid.node(*player_map_pos)
        finder = AStarFinder(diagonal_movement=DiagonalMovement.only_when_no_obstacle)
        path, runs = finder.find_path(start, end, grid)
        if LOG_PATHFINDING:
            logger.debug(grid.grid_str(path, start, end))
        return path


class Slime(Enemy):
    HITBOX_REL_PTS = [
        Coords(1, 7), Coords(3, 2), Coords(5, 2),
        Coords(8, 5), Coords(8, 7)
    ]
    DMG: int = 5
    DMG_INTERVAL: int = 20
    PUSHBACK: int = 1
    hostility: HostilityLevel = HostilityLevel.FRIENDLY

    def __init__(self, position):
        super().__init__({
            'slime_all': SLIME_ALL(),
            'slime_dead': SLIME_DEAD()
        },
            position, 'Slime', hitbox=Hitbox(position, self.HITBOX_REL_PTS, 'GOBLIN_HTBX'))
        self.animation = self.animations.get('slime_all')
        self.health = 10
        self.movement = self.initial_movement = Movement(steps=[
            StepType.RANDOMIZE_DIR,
            StepType.MOVE_TO_DIR,
            StepType.REST
        ], sequence=(1, randint(2, 10), randint(2, 8)), step_time=5)
        self.sight_range = 30

    def receive_dmg(self, dmg: int):
        super().receive_dmg(dmg)
        if not MUTE_SOUNDS :
            pyxel.play(3, SOUNDS['SLIME']['damaged'])
        if self.is_dead:
            self.animation = self.animations.get('slime_dead')
            self.movement.stop_movement()
            return EventType.KILL