from copy import deepcopy

from engine.animated_block import AnimatedBlock
from resources.animations.blocks import CANDLE_STATIC


class Candle(AnimatedBlock):
    def __init__(self, position):
        super().__init__({
            'candle_static': deepcopy(CANDLE_STATIC)
        }, position, 'Standing Candle')
        self.animation = self.animations['candle_static']
