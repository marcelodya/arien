from logging import getLogger

import pyxel

from config import MUTE_SOUNDS
from engine.animated_object import AnimatedObject
from engine.animation import Animation
from engine.helpers.structs import Coords, Hitbox, Direction
from resources.animations.sword import SWORD_N, SWORD_S, SWORD_E, SWORD_W, SWORD_NE, SWORD_SE, SWORD_SW, SWORD_NW
from resources.sounds import SOUNDS

logger = getLogger('Sword')

SWORD_N_OFFSET = Coords(1, -4)
SWORD_S_OFFSET = Coords(0, 5)
SWORD_E_OFFSET = Coords(4, 0)
SWORD_W_OFFSET = Coords(-3, 0)
SWORD_NW_OFFSET = Coords(-2, -1)
SWORD_NE_OFFSET = Coords(5, -1)
SWORD_SW_OFFSET = Coords(-2, 3)
SWORD_SE_OFFSET = Coords(5, 3)


class SwordObject(AnimatedObject):
    HITBOX_REL_PTS = [
        Coords(1, 1), Coords(7, 1), Coords(1, 6),
        Coords(7, 6)
    ]
    LIFETIME: int = 2

    # TODO: Add abstract class for weapon entity objects with these args
    def __init__(self, initial_position: Coords, spawn_time: int, init_direction: Direction):
        if not MUTE_SOUNDS:
            pyxel.play(3, SOUNDS['ARIEN']['hit'])

        if init_direction == Direction.N:
            offset = SWORD_N_OFFSET
            anim = 'SWORD_N'
        elif init_direction == Direction.S:
            offset = SWORD_S_OFFSET
            anim = 'SWORD_S'
        elif init_direction == Direction.W:
            offset = SWORD_W_OFFSET
            anim = 'SWORD_W'
        elif init_direction == Direction.E:
            offset = SWORD_E_OFFSET
            anim = 'SWORD_E'
        elif init_direction == Direction.NW:
            offset = SWORD_NW_OFFSET
            anim = 'SWORD_NW'
        elif init_direction == Direction.NW:
            offset = SWORD_NE_OFFSET
            anim = 'SWORD_NE'
        elif init_direction == Direction.SW:
            offset = SWORD_SW_OFFSET
            anim = 'SWORD_SW'
        elif init_direction == Direction.SE:
            offset = SWORD_SE_OFFSET
            anim = 'SWORD_SE'
        else:
            offset = SWORD_S_OFFSET
            anim = 'SWORD_S'

        real_position: Coords = initial_position + offset

        super().__init__({
            'SWORD_N': SWORD_N(),
            'SWORD_S': SWORD_S(),
            'SWORD_E': SWORD_E(),
            'SWORD_W': SWORD_W(),
            'SWORD_NE': SWORD_NE(),
            'SWORD_NW': SWORD_NW(),
            'SWORD_SW': SWORD_SW(),
            'SWORD_SE': SWORD_SE(),
        }, position=real_position, name='Sword',
            hitbox=Hitbox(real_position, self.HITBOX_REL_PTS, 'SWORD_HTBX'))

        self.spawn_time = spawn_time
        self.animation = self.animations.get(anim)

    def update(self, time: int):
        super(SwordObject, self).update(time)
        if time > self.spawn_time + self.LIFETIME:
            return True
        return None
