from engine.animation import Animation
from engine.helpers.colors import PINK
from engine.helpers.graphic_resources import Tile
from engine.helpers.structs import Coords

CANDLE_STATIC = Animation(
    [
        Tile(Coords(32, 0), 0, PINK),
        Tile(Coords(40, 0), 0, PINK),
        Tile(Coords(32, 8), 0, PINK),
        Tile(Coords(40, 8), 0, PINK),
    ], name='candle_static', time_per_frame=10, if_random=False,
)
