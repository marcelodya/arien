from engine.animation import Animation
from engine.helpers.colors import PINK
from engine.helpers.graphic_resources import Tile
from engine.helpers.structs import Coords

SWORD_N = lambda: Animation(
    [
        Tile(Coords(41, 16), 0, w=7, h=6, transparent=PINK),
        Tile(Coords(33, 16), 0, w=7, h=6, transparent=PINK),
    ], name='SWORD_N', time_per_frame=1, if_random=False, if_looped=False
)
SWORD_S = lambda: Animation(
    [
        Tile(Coords(32, 22), 0, w=7, h=6, transparent=PINK),
        Tile(Coords(40, 22), 0, w=7, h=6, transparent=PINK),
    ], name='SWORD_S', time_per_frame=1, if_random=False, if_looped=False
)
SWORD_W = lambda: Animation(
    [
        Tile(Coords(0, 32), 0, w=7, h=7, transparent=PINK),
        Tile(Coords(8, 32), 0, w=7, h=7, transparent=PINK),
    ], name='SWORD_W', time_per_frame=1, if_random=False, if_looped=False
)
SWORD_E = lambda: Animation(
    [
        Tile(Coords(0, 32), 0, w=-7, h=7, transparent=PINK),
        Tile(Coords(8, 32), 0, w=-7, h=7, transparent=PINK),
    ], name='SWORD_E', time_per_frame=1, if_random=False, if_looped=False
)
SWORD_NW = lambda: Animation(
    [
        Tile(Coords(0, 40), 0, w=5, h=5, transparent=PINK),
        Tile(Coords(5, 40), 0, w=5, h=5, transparent=PINK),
    ], name='SWORD_NW', time_per_frame=1, if_random=False, if_looped=False
)
SWORD_NE = lambda: Animation(
    [
        Tile(Coords(0, 40), 0, w=-5, h=5, transparent=PINK),
        Tile(Coords(5, 40), 0, w=-5, h=5, transparent=PINK),
    ], name='SWORD_NE', time_per_frame=1, if_random=False, if_looped=False
)
SWORD_SW = lambda: Animation(
    [
        Tile(Coords(10, 40), 0, w=5, h=5, transparent=PINK),
        Tile(Coords(16, 32), 0, w=5, h=7, transparent=PINK),
    ], name='SWORD_SW', time_per_frame=1, if_random=False, if_looped=False
)
SWORD_SE = lambda: Animation(
    [
        Tile(Coords(10, 40), 0, w=-5, h=5, transparent=PINK),
        Tile(Coords(16, 32), 0, w=-5, h=7, transparent=PINK),
    ], name='SWORD_S', time_per_frame=1, if_random=False, if_looped=False
)
