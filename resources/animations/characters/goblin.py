from engine.animation import Animation
from engine.helpers.colors import PINK
from engine.helpers.graphic_resources import Tile
from engine.helpers.structs import Coords

GOBLIN_STANDING = Animation(
    [
        Tile(Coords(16, 16), 0, PINK),
        Tile(Coords(16, 16), 0, w=-8, transparent=PINK),
    ], name='goblin_standing', time_per_frame=30, if_random=False,
)
