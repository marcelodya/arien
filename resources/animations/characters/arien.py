from engine.animation import Animation
from engine.helpers.colors import PINK
from engine.helpers.graphic_resources import Tile
from engine.helpers.structs import Coords

ARIEN_STANDING_N = Animation(
    [
        Tile(Coords(48, 0), 0, PINK),
    ], name='arien_standing_n', time_per_frame=1, if_random=False,
)
ARIEN_STANDING_S = Animation(
    [
        Tile(Coords(56, 0), 0, PINK),
    ], name='arien_standing_s', time_per_frame=1, if_random=False,
)
ARIEN_STANDING_E = Animation(
    [
        Tile(Coords(48, 8), 0, PINK),
    ], name='arien_standing_e', time_per_frame=1, if_random=False,
)
ARIEN_STANDING_W = Animation(
    [
        Tile(Coords(48, 8), 0, w=-8, transparent=PINK),
    ], name='arien_standing_w', time_per_frame=1, if_random=False,
)

ARIEN_WALKING_N = Animation(
    [
        Tile(Coords(48, 24), 0, transparent=PINK),
        Tile(Coords(56, 24), 0, transparent=PINK),
    ], name='arien_walking_n', time_per_frame=5,
)
ARIEN_WALKING_S = Animation(
    [
        Tile(Coords(48, 16), 0, transparent=PINK),
        Tile(Coords(56, 16), 0, transparent=PINK),
    ], name='arien_walking_s', time_per_frame=5,
)
ARIEN_WALKING_E = Animation(
    [
        Tile(Coords(48, 8), 0, PINK),
        Tile(Coords(56, 8), 0, PINK),
    ], name='arien_walking_e', time_per_frame=5,
)
ARIEN_WALKING_W = Animation(
    [
        Tile(Coords(48, 8), 0, w=-8, transparent=PINK),
        Tile(Coords(56, 8), 0, w=-8, transparent=PINK),
    ], name='arien_walking_w', time_per_frame=5,
)
ARIEN_WALKING_NE = Animation(
    [
        Tile(Coords(0, 16), 0, transparent=PINK),
        Tile(Coords(8, 16), 0, transparent=PINK),
    ], name='arien_walking_ne', time_per_frame=5,
)
ARIEN_WALKING_NW = Animation(
    [
        Tile(Coords(0, 16), 0, w=-8, transparent=PINK),
        Tile(Coords(8, 16), 0, w=-8, transparent=PINK),
    ], name='arien_walking_nw', time_per_frame=5,
)
ARIEN_WALKING_SE = Animation(
    [
        Tile(Coords(0, 24), 0, transparent=PINK),
        Tile(Coords(8, 24), 0, transparent=PINK),
    ], name='arien_walking_se', time_per_frame=5,
)
ARIEN_WALKING_SW = Animation(
    [
        Tile(Coords(0, 24), 0, w=-8, transparent=PINK),
        Tile(Coords(8, 24), 0, w=-8, transparent=PINK),
    ], name='arien_walking_sw', time_per_frame=5,
)
