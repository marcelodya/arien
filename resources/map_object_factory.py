from typing import Any

from engine.helpers.structs import MapTileType
from resources.blocks.walls import walls_factory
from resources.tiles.floors import DIRT_FLOOR

MAPPING = {
    MapTileType.EMPTY_SPACE : DIRT_FLOOR,
    MapTileType.WALL : walls_factory,
}


def map_object_factory(tile_type: MapTileType) -> Any:
    return MAPPING.get(tile_type)
