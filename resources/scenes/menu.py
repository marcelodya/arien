import pyxel

from config import MUTE_MUSIC
from engine.helpers.structs import Coords, SceneActions
from engine.scenes import Scene, MenuEntry
from resources.sounds import SOUNDS, MUSIC


class Menu(Scene):
    color = 7
    hl_col = 3
    back_col = 5

    def __init__(self):
        self.entries = [
            MenuEntry(Coords(pyxel.width // 3 + 1, pyxel.height // 3), 'NEW GAME', SceneActions.NEW_GAME),
            MenuEntry(Coords(pyxel.width // 3, pyxel.height // 3 + 10), 'LOAD GAME', SceneActions.LOAD_GAME),
            MenuEntry(Coords(pyxel.width // 3, pyxel.height // 3 + 20), 'EXIT GAME', SceneActions.EXIT_GAME),
        ]
        self.active_entry = 0
        if not MUTE_MUSIC:
            pyxel.playm(MUSIC['MENU'][0][0], loop=MUSIC['THEME'][0][1])

        super().__init__('MAIN MENU')

    def up(self):
        self.active_entry -= 1
        if self.active_entry < 0:
            self.active_entry = len(self.entries) - 1

    def down(self):
        self.active_entry += 1
        if self.active_entry == len(self.entries):
            self.active_entry = 0

    def draw(self):
        for nr, e in enumerate(self.entries):
            if nr == self.active_entry:
                pyxel.text(e.pos.x + 2, e.pos.y + 1, e.text, self.hl_col)
            else:
                pyxel.text(e.pos.x + 1, e.pos.y + 1, e.text, self.back_col)
            pyxel.text(e.pos.x, e.pos.y, e.text, self.color)

    def update(self):
        if pyxel.btnp(pyxel.KEY_UP):
            pyxel.play(3, SOUNDS['MENU']['move'])
            self.up()
        elif pyxel.btnp(pyxel.KEY_DOWN):
            pyxel.play(3, SOUNDS['MENU']['move'])
            self.down()
        elif pyxel.btnp(pyxel.KEY_SPACE):
            pyxel.play(3, SOUNDS['MENU']['click'])
            return self.entries[self.active_entry].action

        return None

        # if pyxel.btnp(pyxel.KEY_1):
        #     self.color +=1
        #     if self.color == 16:
        #         self.color = 0

        # elif pyxel.btnp(pyxel.KEY_2):
        #     self.hl_col +=1
        #     if self.hl_col == 16:
        #         self.hl_col = 0

        # elif pyxel.btnp(pyxel.KEY_3):
        #     self.back_col +=1
        #     if self.back_col == 16:
        #         self.back_col = 0
