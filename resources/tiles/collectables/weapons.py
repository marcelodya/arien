from engine.helpers.colors import PINK
from engine.helpers.graphic_resources import Tile
from engine.helpers.structs import Coords

BASIC_SWORD = Tile(Coords(8, 0), w=6, h=6, bank_id=1, transparent=PINK)