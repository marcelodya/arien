# TODO: Create namedtuples for sounds and music

SOUNDS = {
    'MENU' : {
        'move' : 31,
        'click' : 32,
    },
    'ARIEN' : {
        'walk' : 30,
        'hit' : 33,
        'damaged' : 35
    },
    'SLIME' : {
        'damaged' : 34
    }
}

MUSIC = {
    'THEME' : [
        (1, True)
    ],
    'MENU' : [
        (0, True)
    ]
}