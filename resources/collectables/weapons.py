from engine.collectible import WeaponCollectible, WeaponStats
from engine.helpers.structs import CollectableRarityType
from entities.sword_object import SwordObject
from resources.tiles.collectables.weapons import BASIC_SWORD

AVAILABLE_WEAPONS = {
    "BASIC_SWORD": WeaponCollectible(
        name="Basic sword",
        description="Starting weapon",
        representation=BASIC_SWORD,
        rarity=CollectableRarityType.COMMON,
        stats=WeaponStats(power=2, pushback=3),
        animated_object_cls=SwordObject
    )
}
