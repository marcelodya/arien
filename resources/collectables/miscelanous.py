from engine.collectible import Collectible
from engine.helpers.structs import CollectableRarityType
from resources.tiles.collectables.miscelanous import SLIME_GOO_TILE

MISC_COLLECTABLES = {
    'SLIME_GOO': Collectible(name='Slime goo', description='Sticky, stinky..does nothing',
                             representation=SLIME_GOO_TILE,
                             rarity=CollectableRarityType.COMMON),
    'UNCMN_SLIME_GOO': Collectible(name='UNCMN Slime goo', description='Sticky, stinky..does nothing',
                                   representation=SLIME_GOO_TILE, rarity=CollectableRarityType.UNCOMMON),
    'RARE_SLIME_GOO': Collectible(name='RARE Slime goo', description='Sticky, stinky..does nothing',
                                  representation=SLIME_GOO_TILE, rarity=CollectableRarityType.RARE),
    'EPIC_SLIME_GOO': Collectible(name='EPIC Slime goo', description='Sticky, stinky..does nothing',
                                  representation=SLIME_GOO_TILE, rarity=CollectableRarityType.EPIC)
}
