import configparser
import os

CONFIG_FILE = os.path.join(os.path.dirname(__file__), 'arien.ini')

config = configparser.ConfigParser()
config.read(CONFIG_FILE)

DEBUG = config['MAIN'].getboolean('DEBUG')
HITBOX_COLOR = config['MAIN'].getint('HITBOX_COLOR')
SHOW_HITBOXES = config['MAIN'].getboolean('SHOW_HITBOXES')
SHOW_HTBX_NAMES = config['MAIN'].getboolean('SHOW_HITBOX_NAMES')
SHOW_CAM_HTBX = config['MAIN'].getboolean('SHOW_CAMERA_HITBOX')
SHOW_TRIGGERS = config['MAIN'].getboolean('SHOW_TRIGGERS')
SHOW_SIGHT_RANGE = config['MAIN'].getboolean('SHOW_SIGHT_RANGE')
SHOW_ACTIVE_CLUSTERS = config['MAIN'].getboolean('SHOW_ACTIVE_CLUSTERS')

PRINT_MAP_GENERATION = config['MAIN'].getboolean('PRINT_MAP_GENERATION')
LOG_PATHFINDING = config['MAIN'].getboolean('LOG_PATHFINDING')

MUTE_MUSIC = config['SOUND'].getboolean('MUTE_MUSIC')
MUTE_SOUNDS = config['SOUND'].getboolean('MUTE_SOUNDS')

ENEMY_RESPAWN_RATIO = config['GAMEPLAY'].getint('ENEMY_RESPAWN_RATIO')
DROP_RATE = config['GAMEPLAY'].getfloat('DROP_RATE')
