import random
from collections import namedtuple
from enum import Enum
from typing import List, Callable, Any

import numpy as np
import pyxel
from matplotlib.patches import Polygon

from config import HITBOX_COLOR, SHOW_HTBX_NAMES


class Coords:
    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y

    @property
    def tuple(self):
        return (self.x, self.y)

    @property
    def list(self):
        return [self.x, self.y]

    def between(self, oth1, oth2):
        l_x, r_x = sorted((oth1.x, oth2.x))
        l_y, r_y = sorted((oth1.y, oth2.y))
        if l_x <= self.x <= r_x and l_y <= self.y <= r_y:
            return True
        return False

    def __add__(self, other):
        if type(other) == Direction:
            other = other.value
        return Coords(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Coords(self.x - other.x, self.y - other.y)

    def __mul__(self, other):
        t = type(other)
        if t == int:
            return Coords(self.x * other, self.y * other)
        elif t == Coords:
            return Coords(self.x * other.x, self.y * other.y)

    def __eq__(self, other):
        if self.x == other.x and self.y == other.y:
            return True
        return False

    def __str__(self):
        return f"({self.x}x{self.y})"

    __radd__ = __add__
    __repr__ = __str__


class ResourceTypes(Enum):
    TILE = 1
    TILE_MAP = 2
    MUSIC = 3


class Direction(Enum):
    N = Coords(0, -1)
    NW = Coords(-1, -1)
    W = Coords(-1, 0)
    SW = Coords(-1, 1)
    S = Coords(0, 1)
    SE = Coords(1, 1)
    E = Coords(1, 0)
    NE = Coords(1, -1)


class Hitbox:
    def __init__(
        self, absolute_rel_point: Coords, points: List[Coords], name: str = None
    ):
        self.points = points
        self.update(absolute_rel_point)
        self.name = name

    @property
    def relative_points(self):
        return [p + self.absolute_rel_point for p in self.points]

    @property
    def path(self):
        return self._path

    def update(self, absolute_rel_point):
        self.absolute_rel_point = absolute_rel_point
        self._path = Polygon(
            self.ccw_sort([(p.x, p.y) for p in self.relative_points])
        ).get_path()

    def if_intersects(self, other):
        if isinstance(other, Hitbox):
            other = other.path
        return self._path.intersects_path(other)

    def draw(self, cam_pos: Coords):
        last_v = None
        for v in self.path.vertices:
            v = [v[0] - cam_pos.x, v[1] - cam_pos.y]
            if last_v is not None:
                pyxel.line(last_v[0], last_v[1], v[0], v[1], int(HITBOX_COLOR))
            last_v = v
        if SHOW_HTBX_NAMES:
            text_x = (
                max(self.path.vertices[0][0], self.path.vertices[1][0])
                - min(self.path.vertices[0][0], self.path.vertices[1][0])
            ) / 2 - cam_pos.x
            text_y = (
                max(self.path.vertices[0][1], self.path.vertices[1][1])
                - min(self.path.vertices[0][1], self.path.vertices[1][1])
            ) / 2 - cam_pos.y
            pyxel.text(text_x, text_y, self.name, int(HITBOX_COLOR))

    def __str__(self):
        return f"Hitbox with vertices: {self.path.vertices}"

    # This vertices sorting func comes from https://stackoverflow.com/a/44143444
    @staticmethod
    def ccw_sort(p):
        p = np.array(p)
        mean = np.mean(p, axis=0)
        d = p - mean
        s = np.arctan2(d[:, 0], d[:, 1])
        return p[np.argsort(s), :]


class SceneActions(Enum):
    NEW_GAME = 1
    LOAD_GAME = 2
    EXIT_GAME = 3
    INVENTORY = 4
    RESUME_GAME = 5


NPCInfo = namedtuple("NPCInfo", "radius, text, action, trigger_keys")


class InfoAction:
    def __init__(self, steps: List[Callable]):
        self.steps = steps
        self.step_no = 0
        self.current_step = None

    def next(self, time: int, *args, **kwargs):
        if self.step_no >= len(self.steps):
            return None
        self.current_step = self.steps[self.step_no](time=time)
        self.step_no += 1
        return self.current_step


class MapTileType(Enum):
    EMPTY_SPACE = 0
    WALL = 1
    DOORS = 2
    ENTRY = 3
    EXIT = 4
    FLOOD_FILL = "◦"


class MapSkeleton:
    def __init__(self, width: int, height: int):
        self._map = None
        self.width = width
        self.height = height

    def randomize(self, values: List[MapTileType] = [], ratio: float = 0.4):
        self._map = [
            [
                random.choice(values)
                if values
                else MapTileType.WALL
                if random.random() <= ratio
                else MapTileType.EMPTY_SPACE
                for _ in range(self.height)
            ]
            for _ in range(self.width)
        ]

    @property
    def as_list(self):
        return self._map

    def as_custom_list(
        self,
        parse_fn: Callable[[MapTileType], Any] = lambda t: int(
            t is MapTileType.EMPTY_SPACE
        ),
        scale: int = 1,
        swap_x_y: bool = False
    ):
        return [
            [
                parse_fn(self._map[y][x] if swap_x_y else self._map[x][y])
                for y in range(self.width if swap_x_y else self.height)
                for _ in range(scale)
            ]
            for x in range(self.height if swap_x_y else self.width)
            for _ in range(scale)
        ]

    def __str__(self):
        return f"Map<x:{self.width},y:{self.height}>"

    def __getitem__(self, item: Coords):
        if item.x < 0 or item.x >= self.width or item.y < 0 or item.x >= self.width:
            raise ValueError(f"{item} does not exist in {self}")
        return self._map[item.x][item.y]

    def __setitem__(self, key: Coords, value: MapTileType):
        if key.x < 0 or key.x >= self.width or key.y < 0 or key.x >= self.width:
            raise ValueError(f"{key} does not exist in {self}")
        self._map[key.x][key.y] = value

    def __iter__(self):
        for x in range(self.width):
            for y in range(self.height):
                yield self._map[x][y], Coords(x, y)


class EventType(Enum):
    KILL = 1
    START_QUEST = 2
    END_QUEST = 3
    INFO = 4


Event = namedtuple("Event", "type, target")

QuestObjective = namedtuple("QuestObjective", "event_type, quantity, target")


class CollectableRarityType(Enum):
    COMMON = 0.5
    UNCOMMON = 0.3
    RARE = 0.175
    EPIC = 0.025
