import pyxel

from engine.helpers.colors import PINK
from engine.helpers.structs import ResourceTypes, Coords


class GraphicResourceBase:
    """Base class for all the tiles and tilemaps.
    Sets default width, height and transparent color.
    """
    w: int = 8
    h: int = 8
    transparent: int = PINK

    def __init__(self, r_type: ResourceTypes,
                 coords: Coords, bank_id: int, transparent: int = None,
                 w: int = None, h: int = None):
        self.x = coords.x
        self.y = coords.y
        self.transparent = transparent if transparent else self.transparent
        self.type = r_type
        self.bank_id = bank_id
        self.w = w if w else self.w
        self.h = h if h else self.h

    def draw(self, coords: Coords, mirror_x: bool = False, mirror_y: bool = False):
        """Draws the resource to given screen position.

        Parameters
        ----------
        screen_coords - Coords of the point on the screen
        mirror_x - If the image should be mirrored relative to the X axis
        mirror_y - If the image should be mirrored relative to the Y axis
        """
        drawing_fn = pyxel.blt if self.type == ResourceTypes.TILE else pyxel.bltm if self.type == ResourceTypes.TILE_MAP else None

        if drawing_fn is None:
            raise ValueError('Incorrect resource type: ', self.type)
        # print(drawing_fn)
        drawing_fn(coords.x, coords.y, self.bank_id, self.x, self.y, self.w, self.h, colkey=14)
        # bltm(0,0,0,0,0,32,32)

    def __str__(self):
        return f"""{self.type} of bank {self.bank_id}, x: {self.x}, y: {self.y}, 
w: {self.w}, h: {self.h}, transparent: {self.transparent}"""


class Tile(GraphicResourceBase):
    def __init__(self, coords, bank_id, transparent=None, w=8, h=8):
        super().__init__(ResourceTypes.TILE, coords, bank_id, transparent=transparent, w=w, h=h)


class TileMap(GraphicResourceBase):
    def __init__(self, coords, bank_id, transparent=None, w=80, h=80):
        super().__init__(ResourceTypes.TILE_MAP, coords, bank_id, transparent=transparent, w=w, h=h)

    def draw(self, coords: Coords, mirror_x: bool = False, mirror_y: bool = False):
        super().draw(coords)
