from enum import Enum
from logging import getLogger
from random import choice
from typing import Callable, List, Tuple, Sequence

from pathfinding.core.diagonal_movement import DiagonalMovement
from pathfinding.core.grid import Grid
from pathfinding.finder.a_star import AStarFinder

from config import LOG_PATHFINDING
from engine.helpers.structs import Direction, MapSkeleton, Coords

logger = getLogger(__name__)


class StepType(Enum):
    RANDOMIZE_DIR = 0
    MOVE_TO_DIR = 1
    REST = 2


class Movement:
    def __init__(self, steps: List[StepType], sequence: Sequence[int], step_time: int):
        self.steps = steps
        self.sequence = sequence
        self.step_time = step_time
        self.steps_no = len(steps)

        self.current_step = 0
        self.current_sequence_value = sequence[0]

        self.current_dir = Direction.N
        self.start_time = None

        self.is_moving = True

    def start_movement(self, start_time: int):
        self.start_time = start_time

    def stop_movement(self):
        self.is_moving = False

    def next_move(self, moving_fn: Callable, time: int, would_collide: bool):
        curr_step = self.steps[self.current_step]
        time_diff = time - self.start_time

        if not self.is_moving or not time_diff or time_diff % self.step_time:
            return

        if curr_step == StepType.RANDOMIZE_DIR:
            self.__randomize_dir()
        elif curr_step == StepType.MOVE_TO_DIR:
            if would_collide:
                self.__randomize_dir()
            moving_fn(self.current_dir)
        elif curr_step == StepType.REST:
            pass

        self.__check_sequence()

    def __check_sequence(self):
        self.current_sequence_value = max(self.current_sequence_value - 1, 0)
        if not self.current_sequence_value:
            self.current_step += 1
            if self.current_step >= self.steps_no:
                self.current_step = 0
            self.current_sequence_value = self.sequence[self.current_step]

    def __randomize_dir(self):
        self.current_dir = choice(list(Direction))


def get_path_to_position(
    source_coords: Coords,
    target_coords: Coords,
    map_skeleton: MapSkeleton,
    map_tile_size: int = 8,
) -> List[Tuple[int, int]]:
    scaled_map = map_skeleton.as_custom_list(scale=2, swap_x_y=True)
    grid = Grid(matrix=scaled_map)
    src_map_pos = source_coords.x // map_tile_size, source_coords.y // map_tile_size
    target_map_pos = target_coords.x // map_tile_size, target_coords.y // map_tile_size
    start = grid.node(*src_map_pos)
    end = grid.node(*target_map_pos)
    finder = AStarFinder(diagonal_movement=DiagonalMovement.only_when_no_obstacle)
    path, runs = finder.find_path(start, end, grid)
    if LOG_PATHFINDING:
        logger.debug(grid.grid_str(path, start, end))
    return path
