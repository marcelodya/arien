import os
import random
from logging import getLogger
from time import sleep

from config import PRINT_MAP_GENERATION
from engine.helpers.structs import MapSkeleton, MapTileType, Coords, Direction

SLEEP_TIME = 0.01
logger = getLogger(__name__)


def flood(start_pos: Coords, map_skeleton: MapSkeleton):
    map_skeleton[start_pos] = MapTileType.FLOOD_FILL
    if PRINT_MAP_GENERATION and SLEEP_TIME:
        sleep(SLEEP_TIME)
        MapGenerator.print_map(map_skeleton)
    n = start_pos + Direction.N
    s = start_pos + Direction.S
    w = start_pos + Direction.W
    e = start_pos + Direction.E
    if map_skeleton[n] == MapTileType.EMPTY_SPACE:
        flood(n, map_skeleton)
    if map_skeleton[s] == MapTileType.EMPTY_SPACE:
        flood(s, map_skeleton)
    if map_skeleton[w] == MapTileType.EMPTY_SPACE:
        flood(w, map_skeleton)
    if map_skeleton[e] == MapTileType.EMPTY_SPACE:
        flood(e, map_skeleton)


def get_empty_spots(map_skel):
    empty_spots = []
    for field, coords in map_skel:
        if field == MapTileType.EMPTY_SPACE:
            empty_spots.append(coords)
    return empty_spots


def connect_isolated_areas(map_skeleton: MapSkeleton):
    empty_spots = get_empty_spots(map_skeleton)
    if not empty_spots:
        logger.warning("No empty spots on the map skeleton found!")
        return

    start_coords = random.choice(empty_spots)
    flood(start_coords, map_skeleton)
    isolated = get_empty_spots(map_skeleton)
    while len(isolated):
        # TODO: Maybe adding some treasure in here?
        isolated_random_point = random.choice(isolated)
        MapGenerator.connect_points(start_coords, isolated_random_point, map_skeleton)
        flood(isolated_random_point, map_skeleton)
        isolated = get_empty_spots(map_skeleton)

    # Cleanup after flooding
    for f, c in map_skeleton:
        if f == MapTileType.FLOOD_FILL:
            map_skeleton[c] = MapTileType.EMPTY_SPACE


class MapGenerator:
    @staticmethod
    def _count_active_neighbours(map_skeleton: MapSkeleton, pos: Coords):
        active_no = 0
        for x in range(-1, 2):
            for y in range(-1, 2):
                new_x = pos.x + x
                new_y = pos.y + y
                if new_x == 0 and new_y == 0:
                    continue
                if (
                    new_x < 0
                    or new_x >= map_skeleton.width
                    or new_y < 0
                    or new_y >= map_skeleton.height
                ):
                    active_no += 1
                elif map_skeleton[Coords(new_x, new_y)] == MapTileType.WALL:
                    active_no += 1
        return active_no

    @classmethod
    def _sim_step(
        cls,
        map_skeleton: MapSkeleton,
        x_tiles_no: int,
        y_tiles_no: int,
        walls_limit: int,
        empty_limit: int,
    ):
        new_map = MapSkeleton(x_tiles_no, y_tiles_no)
        new_map.randomize([MapTileType.EMPTY_SPACE])
        for x in range(map_skeleton.width):
            for y in range(map_skeleton.height):
                coords = Coords(x, y)
                alive_no = cls._count_active_neighbours(map_skeleton, coords)
                if map_skeleton[coords] == MapTileType.WALL:
                    new_map[coords] = (
                        MapTileType.EMPTY_SPACE
                        if alive_no < walls_limit
                        else MapTileType.WALL
                    )
                else:
                    new_map[coords] = (
                        MapTileType.WALL
                        if alive_no > empty_limit
                        else MapTileType.EMPTY_SPACE
                    )
        return new_map

    @classmethod
    def procedural_gen_list(
        cls,
        width: int,
        height: int,
        tile_size: int = 8,
        empty_limit: int = 5,
        walls_limit: int = 4,
        steps: int = 6,
        set_boundaries: bool = True,
    ):
        x_tiles_no = width // tile_size
        y_tiles_no = height // tile_size

        map_skeleton = MapSkeleton(x_tiles_no, y_tiles_no)
        map_skeleton.randomize(ratio=0.4)

        for i in range(steps):
            if PRINT_MAP_GENERATION:
                MapGenerator.print_map(map_skeleton)
                sleep(1)
            map_skeleton = cls._sim_step(
                map_skeleton, x_tiles_no, y_tiles_no, walls_limit, empty_limit
            )

        if set_boundaries:
            for x in range(x_tiles_no):
                map_skeleton[Coords(x, 0)] = MapTileType.WALL
                map_skeleton[Coords(x, y_tiles_no - 1)] = MapTileType.WALL
            for y in range(1, y_tiles_no - 1):
                map_skeleton[Coords(0, y)] = MapTileType.WALL
                map_skeleton[Coords(x_tiles_no - 1, y)] = MapTileType.WALL
        if PRINT_MAP_GENERATION:
            MapGenerator.print_map(map_skeleton)
        connect_isolated_areas(map_skeleton)
        return map_skeleton

    @staticmethod
    def print_map(map_skel: MapSkeleton):
        os.system("clear")
        for y in range(map_skel.height):
            for x in range(map_skel.width):
                print(
                    map_skel[Coords(x, y)].value
                    if map_skel[Coords(x, y)].value
                    else " ",
                    end="",
                )
            print("")

    @staticmethod
    def connect_points(p1: Coords, p2: Coords, map_skeleton: MapSkeleton):
        x_dist = p1.x - p2.x
        y_dist = p1.y - p2.y
        path_point = p1
        while not (path_point.x == p2.x and path_point.y == p2.y):
            if (x_dist and random.random() < 0.5) or y_dist == 0:
                x_dist += 1 if x_dist < 0 else -1
            elif y_dist:
                y_dist += 1 if y_dist < 0 else -1
            path_point = Coords(p2.x + x_dist, p2.y + y_dist)
            if map_skeleton[path_point] == MapTileType.WALL:
                map_skeleton[path_point] = MapTileType.FLOOD_FILL
            if PRINT_MAP_GENERATION and SLEEP_TIME:
                print(p1, p2, x_dist, y_dist, path_point)
                sleep(SLEEP_TIME * 5)
                MapGenerator.print_map(map_skeleton)
