from copy import deepcopy
from logging import getLogger
from typing import Callable

import pyxel

from engine.animated_block import AnimatedBlock
from engine.helpers.graphic_resources import Tile
from engine.helpers.structs import MapSkeleton, Coords

logger = getLogger('ObjectMap')


class ObjectMap:
    def __init__(self, object_factory: Callable, tile_size: int):
        self.factory = object_factory
        self.x_size = None
        self.y_size = None
        self.tile_size = tile_size
        self._map = None

    def init_objects(self, map_skeleton: MapSkeleton):
        logger.info('Populating map')
        self.x_size = map_skeleton.width
        self.y_size = map_skeleton.height
        self._map = deepcopy(map_skeleton.as_list)
        for x in range(self.x_size):
            for y in range(self.y_size):
                tile_type = self._map[x][y]
                res = self.factory(tile_type=tile_type)
                if isinstance(res, Tile):
                    self._map[x][y] = res
                else:
                    self._map[x][y] = res(position=Coords(x, y) * self.tile_size, tile_type=tile_type, tile_size=16)

    def draw(self, camera_position: Coords):
        for x in range(self.x_size):
            for y in range(self.y_size):
                coords = Coords(x, y)
                relative_coords = coords * self.tile_size - camera_position

                # Not efficient
                # if relative_coords.between(Coords(-8,-8), Coords(pyxel.width + 8, pyxel.height + 8)) and self.SOLID_MAP[Coords(x, y)]:
                if (-self.tile_size < relative_coords.x < pyxel.width + self.tile_size
                        and - self.tile_size < relative_coords.y < pyxel.height + self.tile_size):

                    tile = self._map[x][y]
                    if tile is None:
                        continue
                    t_w = tile.w if isinstance(tile, Tile) else tile.animation.frame.w
                    t_h = tile.h if isinstance(tile, Tile) else tile.animation.frame.h
                    scale_x = self.tile_size // t_w
                    scale_y = self.tile_size // t_h
                    for d_x in range(scale_x):
                        for d_y in range(scale_y):
                            new_coords = Coords(relative_coords.x + d_x * t_h, relative_coords.y + d_y * t_h)
                            if isinstance(tile, Tile):
                                tile.draw(new_coords)
                            elif isinstance(tile, AnimatedBlock):
                                tile.draw(new_coords)

    @property
    def map(self):
        return self._map

    def __iter__(self):
        for x in range(self.x_size):
            for y in range(self.y_size):
                yield self._map[x][y], Coords(x, y)
