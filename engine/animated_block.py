from engine.animated_object import AnimatedObject
from engine.helpers.structs import Coords


class AnimatedBlock(AnimatedObject):
    def draw(
        self,
        relative_position: Coords = None,
        tile_size: int = 16,
        camera_position: Coords = None,
    ):
        """
        TODO: Refactor to set size of object independently of animation tile sizes and allow to draw whole tile block inside this method and not in the game logic
        TODO: Use tile_size or self.tile_size to determine how many repetitions of tile draws should there occur
        """
        if camera_position:
            self._current_animation.frame.draw(self.position - camera_position)
        else:
            self._current_animation.frame.draw(relative_position)
