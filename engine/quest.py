from logging import getLogger

from engine.helpers.structs import QuestObjective, Event, EventType

logger = getLogger('Quest')

class Quest:
    def __init__(self, objective: QuestObjective, effect: EventType, name: str):
        self.objective = objective
        self.effect = effect
        self.name = name

        self.solved = False
        self.state = {
            'quantity' : 0
        }

    def check_event(self, event: Event):
        if event.type == self.objective.event_type and event.target == self.objective.target:
            self.state['quantity'] += 1
            if self.state['quantity'] == self.objective.quantity:
                self.solved = True
                logger.info(f'{self} solved')
                logger.info(f'Spawning effect {self.effect}')
                return self.effect

