from collections import namedtuple
from queue import Queue

import pyxel

from engine.helpers.colors import WHITE, BLACK, PINK
from engine.helpers.statics import PYXEL_LETTER_WIDTH
from engine.helpers.structs import Coords


class TextPopup:
    PADDING = 3

    def __init__(self, position: Coords, width: int, height: int):
        self.position = position
        self.height = height
        self.width = width

        self.text_coords = position + Coords(self.PADDING, self.PADDING)

        self.text = ''
        self._visible = False

    def show(self, *args):
        self._visible = True

    def hide(self):
        self._visible = False
        self.text = ''

    def draw(self, *args, **kwargs):
        if not self.visible:
            return
        pyxel.rect(self.position.x, self.position.y, self.position.x + self.width, self.position.y + self.height, WHITE)
        pyxel.text(self.text_coords.x, self.text_coords.y, self.text, BLACK)

    @property
    def visible(self):
        return self._visible

    @visible.setter
    def visible(self, val: bool):
        self._visible = val


InfoBarData = namedtuple('InfoBarData', 'text, speed, timeout')


class InfoBar(TextPopup):
    SCROLLING_SPEED = 5
    SCROLLING_BEGIN_TIMEOUT = 40

    def __init__(self, *args, timeout: int = 60, scrollable: bool = True, **kwargs):
        super().__init__(*args, **kwargs)

        self.timeout = timeout
        self.scrollable = scrollable
        self.current_char = 0
        self.speed = self.SCROLLING_SPEED
        self.scrolling_timeout = self.SCROLLING_BEGIN_TIMEOUT
        self.scrolling = False
        self.queue = Queue()

        self.start_time = None

    @property
    def onscreen_text_length(self):
        return (self.width - (2 * self.PADDING)) // PYXEL_LETTER_WIDTH

    def show(self, text: str, time: int, speed: int = None, scrolling_timeout: int = None,
             begin_from_right: bool = False):
        if begin_from_right:
            text = ' ' * self.onscreen_text_length + text
        self.queue.put(InfoBarData(text, speed, scrolling_timeout))

    def __get_next_info(self, time):
        info_bar_data = self.queue.get()
        self.current_char = 0
        self.start_time = time
        self.speed = info_bar_data.speed if info_bar_data.speed else self.SCROLLING_SPEED
        self.scrolling_timeout = info_bar_data.timeout if info_bar_data.timeout else self.SCROLLING_BEGIN_TIMEOUT
        self.text = info_bar_data.text

    def update(self, time: int):
        if not self.queue.empty() and not self.visible and not self.scrolling:
            self.visible = True
            self.__get_next_info(time=time)

        if not self.visible:
            return

        t_delta = (time - self.start_time)

        if not self.scrollable and t_delta >= self.timeout:
            self.hide()

        if self.scrollable and not self.scrolling and t_delta >= self.scrolling_timeout:
            self.scrolling = True

        if self.scrolling and not t_delta % self.speed:
            self.current_char += 1
            if self.current_char >= len(self.text):
                self.scrolling = False
                if self.queue.empty():
                    self.hide()
                else:
                    self.__get_next_info(time=time)

    def draw(self, *args, **kwargs):
        if not self.visible:
            return
        pyxel.rect(self.position.x, self.position.y, self.position.x + self.width, self.position.y + self.height, BLACK)

        if not self.scrollable:
            pyxel.text(self.text_coords.x, self.text_coords.y, self.text, PINK)
        else:
            pyxel.text(self.text_coords.x, self.text_coords.y,
                       self.text[self.current_char:self.current_char + self.onscreen_text_length], PINK)
