import textwrap
from logging import getLogger
from math import ceil
from pprint import pprint
from typing import List, Callable

import pyxel

from engine.helpers.colors import WHITE
from engine.helpers.graphic_resources import Tile
from engine.helpers.statics import PYXEL_LETTER_WIDTH, PYXEL_LETTER_HEIGHT
from engine.helpers.structs import Coords, Event

TL = 0
T = 1
TR = 2
L = 3
M = 4
R = 5
BL = 6
B = 7
BR = 8

logger = getLogger('Dialogue')


class Dialogue:
    def __init__(self, tiles: List[Tile], text: str, time: int, width: int = 140, height: int = 40,
                 letter_interval: int = 2, padding: int = 5, blink_interval: int = 15, on_finish_action: Event = None):
        """

        Parameters
        ----------
        width
        height
        tiles
        letter_interval
            Should contain 9 Tiles for each row of the textbox
        """
        self.width = width
        self.height = height
        self.interval = letter_interval
        self.padding = padding
        self.tiles = tiles
        self.text = text
        self.start_time = time
        self.on_finish_action = on_finish_action

        self.x = (pyxel.width - self.width) // 2
        self.y = (pyxel.height - self.height)
        self.TL_coords = Coords(self.x, self.y)
        self.L_coords = self.TL_coords + Coords(0, self.tiles[TL].h)
        self.BL_coords = self.L_coords + Coords(0, self.tiles[L].h)
        self.border_x_s = self.tiles[L].w + self.tiles[R].w
        self.border_y_s = self.tiles[T].h + self.tiles[B].h
        self.text_start_coords = self.TL_coords + Coords(self.padding, self.padding)
        self.text_max_length = ((self.width - self.border_x_s - self.padding) // PYXEL_LETTER_WIDTH)
        self.max_text_lines = ((self.height - self.border_y_s - self.padding * 2) // PYXEL_LETTER_HEIGHT)
        self.wrapped = textwrap.wrap(self.text, self.text_max_length)
        self._paragraphs = [
            '\n'.join(self.wrapped[i * self.max_text_lines:(i + 1) * self.max_text_lines])
            for i in range(0, ceil(len(self.wrapped) / self.max_text_lines))]
        if self.max_text_lines < 1:
            raise ValueError(f'Height of the textbox is too low ! {self.height}')

        self.blink_interval = blink_interval

        logger.debug(f'Initing dialogue in {self.x} x {self.y}')

        self._rendering_text = True
        self._waiting = False
        self._current_char = 0
        self._current_paragraph = 0
        self.blinker = True

    def update(self, time: int):
        t_interval = not (self.start_time - time) % self.interval
        b_interval = not (self.start_time - time) % self.blink_interval
        if self._rendering_text and not self._waiting and t_interval:
            self.__next_letter()
        elif self._waiting and b_interval:
            self.blinker = not self.blinker
        if pyxel.btnp(pyxel.KEY_ENTER) and (self._waiting or self._rendering_text):
            self.blinker = True
            self._current_char = 0
            self._current_paragraph += 1
            self._waiting = False
            if self._current_paragraph >= len(self._paragraphs):
                self._rendering_text = False

            if not self._rendering_text:
                return self.on_finish_action if self.on_finish_action else True

    def __next_letter(self):
        if self._current_char < len(self._paragraphs[self._current_paragraph]):
            self._current_char += 1
        else:
            self._waiting = True

        # if self._current_char == len(
        #         self.wrapped[self._current_row]) and self._current_row and not self._current_row % (
        #         self.max_text_lines - 1):
        #     self._waiting = True
        # elif self._current_char < len(self.wrapped[self._current_row]):
        #     self._current_char += 1
        # elif self._current_row < len(self.wrapped) - 1:
        #     self._current_row += 1
        #     self._current_char = 0
        # else:
        #     self._rendering_text = False
        #     self._waiting = True

    def draw(self):
        if not self._rendering_text and not self._waiting:
            return
        self.tiles[TL].draw(self.TL_coords)
        for x in range(0, (self.width - self.border_x_s) // self.tiles[T].w + 1):
            self.tiles[T].draw(self.TL_coords + Coords(x * self.tiles[T].w + self.tiles[TL].w, 0))
        self.tiles[TR].draw(self.TL_coords + Coords((x + 1) * self.tiles[T].w + self.tiles[TL].w, 0))

        for y in range(self.max_text_lines * PYXEL_LETTER_HEIGHT // self.tiles[M].h + 1):
            row_h = Coords(0, self.tiles[L].h * y)
            self.tiles[L].draw(self.L_coords + row_h)
            for x in range(0, (self.width - self.border_x_s) // self.tiles[M].w + 1):
                self.tiles[M].draw(self.L_coords + Coords(x * self.tiles[M].w + self.tiles[L].w, 0) + row_h)
            self.tiles[R].draw(self.L_coords + Coords((x + 1) * self.tiles[M].w + self.tiles[L].w, 0) + row_h)

        self.tiles[BL].draw(self.BL_coords + row_h)
        for x in range(0, (self.width - self.border_x_s) // self.tiles[B].w + 1):
            self.tiles[B].draw(self.BL_coords + Coords(x * self.tiles[B].w + self.tiles[BL].w, 0) + row_h)
        self.tiles[BR].draw(self.BL_coords + Coords((x + 1) * self.tiles[B].w + self.tiles[BL].w, 0) + row_h)

        pyxel.text(self.text_start_coords.x, self.text_start_coords.y,
                   self._paragraphs[self._current_paragraph][:self._current_char] + (
                       '_' if self.blinker else ''), col=WHITE)
